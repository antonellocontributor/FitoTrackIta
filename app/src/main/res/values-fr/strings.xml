<!--
  ~ Copyright (c) 2020 Jannis Scheibe <jannis@tadris.de>
  ~
  ~ This file is part of FitoTrack
  ~
  ~ FitoTrack is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU General Public License as published by
  ~     the Free Software Foundation, either version 3 of the License, or
  ~     (at your option) any later version.
  ~
  ~     FitoTrack is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU General Public License for more details.
  ~
  ~     You should have received a copy of the GNU General Public License
  ~     along with this program.  If not, see <http://www.gnu.org/licenses/>.
  -->

<resources>
    <string name="app_name">FitoTrack</string>
    <string name="workout_add">Ajouter</string>
    <string name="stop">Arrêter</string>
    <string name="delete">Supprimer</string>
    <string name="exporting">Exportation</string>

    <string name="error">Erreur</string>
    <string name="errorGpxExportFailed">L\'export GPX a échoué.</string>
    <string name="errorExportFailed">L\'export des données a échoué.</string>
    <string name="errorImportFailed">L\'import des données a échoué.</string>
    <string name="shareFile">Partager le fichier</string>
    <string name="initialising">Initialisation</string>
    <string name="preferences">Préférences</string>
    <string name="workouts">Entraînements</string>
    <string name="locationData">Données de localisation</string>
    <string name="converting">Conversion</string>
    <string name="finished">Terminé</string>
    <string name="loadingFile">Chargement du fichier</string>
    <string name="chooseBackupFile">Choisir le fichier de sauvegarde</string>

    <string name="importBackupMessage">ATTENTION: Les données existantes de l\'application seront effacées. Pensez à faire une sauvegarde si vous ne voulez pas les perdre. Êtes-vous sûr de vouloir restaurer cette sauvegarde ?</string>
    <string name="restore">Restaurer</string>
    <string name="backup">Sauvegarder</string>

    <string name="pref_ringtone_silent">Silencieux</string>

    <string name="setPreferencesTitle">Set Preferences</string>
    <string name="setPreferencesMessage">You can set your preferred unit system and your weight in the settings.</string>

    <string name="timeMinuteSingular">Minute</string>
    <string name="timeMinutePlural">Minutes</string>
    <string name="timeMinuteShort">min</string>
    <string name="timeHourSingular">Heure</string>
    <string name="timeHourPlural">Heures</string>
    <string name="timeHourShort">h</string>
    <string name="timeSecondsShort">s</string>
    <string name="and">et</string>

    <string name="errorEnterValidNumber">Merci d\'entrer un nombre valide</string>
    <string name="errorWorkoutAddFuture">L\'entraînement ne peut pas avoir lieu dans le futur.</string>
    <string name="errorEnterValidDuration">Merci d\'entrer une durée valide</string>

    <string name="announcementGPSStatus">Statut GPS</string>

    <string name="voiceAnnouncementsTitle">Annonces vocales</string>

    <string name="pref_voice_announcements_summary">Configurer les annonces vocales utilisées durant l\'entraînement</string>

    <string name="pref_announcements_config_title">Déclenchement des annonces vocales</string>
    <string name="pref_announcements_config_summary">Choisissez le temps et la distance entre les annonces vocales</string>

    <string name="pref_announcements_content">Contenu des annonces vocales</string>

    <string name="gpsLost">Signal GPS perdu</string>
    <string name="gpsFound">Signal GPS trouvé</string>

    <string name="workoutTime">Temps</string>
    <string name="workoutDate">Date</string>
    <string name="workoutDuration">Durée</string>
    <string name="workoutPauseDuration">Durée de la pause</string>
    <string name="workoutStartTime">Heure de début</string>
    <string name="workoutEndTime">Heure de fin</string>
    <string name="workoutDistance">Distance</string>
    <string name="workoutPace">Rythme</string>
    <string name="workoutRoute">Route</string>
    <string name="workoutSpeed">Vitesse</string>
    <string name="workoutAvgSpeedShort">Vitesse moy.</string>
    <string name="workoutAvgSpeedLong">Vitesse moyenne</string>
    <string name="workoutTopSpeed">Vitesse maximale</string>
    <string name="workoutBurnedEnergy">Énergie consommée</string>
    <string name="workoutTotalEnergy">Énergie totale</string>
    <string name="workoutEnergyConsumption">Consommation d\'énergie</string>
    <string name="workoutEdited">L\'entraînement a été édité.</string>
    <string name="uploading">Téléversement</string>
    <string name="enterVerificationCode">Entrez le code de vérification</string>
    <string name="authenticationFailed">Erreur d\'authentification.</string>
    <string name="upload">Téléverser</string>
    <string name="uploadSuccessful">Téléversement réussi</string>
    <string name="uploadFailed">Échec du téléversement</string>
    <string name="uploadFailedOsmNotAuthorized">Non autorisé, essayez encore</string>

    <string name="workoutAscent">Montée</string>
    <string name="workoutDescent">Descente</string>

    <string name="height">Hauteur</string>


    <string name="workoutTypeRunning">Course</string>
    <string name="workoutTypeWalking">Marche</string>
    <string name="workoutTypeJogging">Jogging</string>
    <string name="workoutTypeCycling">Vélo</string>
    <string name="workoutTypeHiking">Randonnée</string>
    <string name="workoutTypeOther">Autre</string>
    <string name="workoutTypeUnknown">Inconnu</string>
    <string name="type">Type</string>

    <string name="enterWorkout">Commencer l\'entraînement</string>

    <string name="setDuration">Définir la durée</string>


    <string name="recordWorkout">Enregistrer l\'entraînement</string>

    <string name="noGpsTitle">GPS désactivé</string>
    <string name="noGpsMessage">Veuillez activer le GPS pour géolocaliser votre entraînement.</string>
    <string name="enable">Activer</string>

    <string name="comment">Commentaire</string>
    <string name="enterComment">Entrez un commentaire</string>
    <string name="okay">Okay</string>

    <string name="stopRecordingQuestion">Arrêter l\'enregistrement ?</string>
    <string name="stopRecordingQuestionMessage">Voulez-vous vraiment arrêter l\'entraînement ?</string>

    <!-- leave the underscore because "continue" is a java command -->
    <string name="continue_">Continuer</string>

    <string name="deleteWorkout">Supprimer l\'entraînement</string>
    <string name="deleteWorkoutMessage">Voulez-vous vraiment supprimer l\'entraînement ?</string>

    <string name="trackerRunning">Tracker en cours</string>
    <string name="trackerRunningMessage">Enregistrement en cours</string>

    <string name="trackingInfo">Info de tracking</string>
    <string name="trackingInfoDescription">Information à propos du fonctionnement du tracker</string>

    <string name="cancel">Annuler</string>
    <string name="exportAsGpxFile">Exporter comme fichier GPX</string>
    <string name="pref_weight">Votre poids</string>
    <string name="pref_weight_summary">Votre poids est nécessaire pour calculer le nombre de calories brûlées</string>
    <string name="pref_unit_system">Système d\'unités préféré</string>
    <string name="settings">Paramètres</string>
    <string name="exportData">Exporter les données</string>
    <string name="exportDataSummary">Effectue une sauvegarde de tous vos entraînements</string>
    <string name="importBackup">Importer une sauvegarde</string>
    <string name="importBackupSummary">Restaurer une sauvegarde</string>
    <string name="gps">GPS</string>
    <string name="data">Données</string>
    <string name="mapStyle">Style de la carte</string>
    <string name="waiting_gps">En attente du GPS</string>
    <string name="actionUploadToOSM">Téléversement sur OSM</string>
    <string name="cut">Enlever les premiers/derniers 300 mètres</string>
    <string name="trackVisibilityPref">Visibilité de la piste</string>
    <string name="description">Description</string>
    <string name="ttsNotAvailable">TextToSpeech n\'est pas disponible </string>
    <string name="action_edit_comment">Modifier le commentaire</string>
    <string name="pref_announcement_mode">Mode d\'annonce</string>

    <string name="save">Enregistrer</string>
    <string name="share">Partager</string>
    <string name="savedToDownloads">Sauvegarder dans Téléchargements</string>
    <string name="savingFailed">Echec de la sauvegarde</string>
    <string name="info">Info</string>
    <string name="OpenStreetMapAttribution">© Contributeurs OpenStreetMap</string>
    <string name="theme">Thème</string>
    <string name="hintRestart">Redémarrer l\'application pour appliquer les modifications</string>
    <string name="noComment">Pas de commentaire</string>

    <string-array name="pref_announcement_mode">
        <item>Toujours</item>
        <item>Seulement avec des écouteurs</item>
    </string-array>

    <string-array name="pref_unit_systems">
        <item>Métrique</item>
        <item>Métrique en m/s</item>
        <item>Impérial</item>
        <item>Impérial en mètre</item>
    </string-array>

    <string-array name="pref_theme_setting">
        <item>Clair</item>
        <item>Sombre</item>
    </string-array>

    <string-array name="osm_track_visibility">
        <item>Identifiable</item>
        <item>Traçable</item>
        <item>Privé</item>
    </string-array>

    <string-array name="pref_map_layers">
        <item>Mapnik\nStyle OSM par défaut</item>
        <item>Humanitaire\nJolie carte mais parfois indisponible</item>
        <item>Extérieur\nAccès limité</item>
        <item>Cyclemap\nAccès limité</item>
    </string-array>


</resources>
